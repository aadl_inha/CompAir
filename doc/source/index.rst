.. CompAir documentation master file, created by
   sphinx-quickstart on Wed Jan 15 16:16:40 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

CompAir
=======

.. toctree::
   :maxdepth: 2
   :caption: Contents:

****
개요
****

CompAir 는?
-----------
CompAir는 압축성 유체역학의 이론식을 Python으로 정리한 패키지이다.
압축성 효과를 고려하여 충격파/팽창파에 따른 물성치 변화와 
노즐 및 관내 유동을 준 1차원으로 해석할 수 있다.
현재 (CompAir |version|)은 다음 기능을 다룬다.

- 수직/경사 충격파 및 팽창파 (:mod:`compair.normal_shock`, :mod:`compair.oblique_shock`, :mod:`compair.prandtl_expand`)
- 등엔트로피 준 1차원 유동 (:mod:`compair.isentropic`, :mod:`compair.nozzle`)
- 표준 대기 특성 (:mod:`compair.atmos1976`)

요구 사항
---------

CompAir |version| 는 Python 3.4+와 아래 패키지들이 설치된 환경에서 작동한다.

1. `numpy` >= 1.8
2. `scipy` >= 0.1

설치
----

CompAir |version| 는 `저장소 <https://gitlab.com/aadl_inha/CompAir>`_ 에서 
소스코드를 다운받을 수 있다. ``setup.py`` 를 이용해서 설치한다::

    user@computer ~/CompAir$ python setup.py install

****
이론
****
.. automodule:: compair.normal_shock

.. automodule:: compair.oblique_shock

.. automodule:: compair.prandtl_expand

.. automodule:: compair.isentropic

.. automodule:: compair.nozzle

.. automodule:: compair.cone_shock

.. automodule:: compair.atmos1976


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
