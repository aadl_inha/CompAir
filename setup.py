from setuptools import setup

# PySMUF version
import compair._version as _version


version = _version.__version__

# Hard dependencies
install_requires = [
    'numpy >= 1.8',
    'scipy >= 0.1'
]

setup(name='CompAir',
      version=version,
      author='Jin Seok Park',
      description='Compressible Air Equations',
      packages=['compair'],
      install_requires=install_requires,)
