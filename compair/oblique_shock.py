r"""
경사 충격파 관계식
====================

2차원 쇄기에서 경사 충격파의 각도와 경사 충격파 전/후 물성치를 계산한다.
압축성 유체역학 교과서 [1] 에 자세한 유도 과정이 설명되어 있다.

Notes
------
각도 :math:`\theta` 를 갖는 2차원 쇄기에 대해 마하수 `M` 과 
경사충격파 각도 :math:`\beta` 는 :math:`\theta-\beta-M` 관계식을 갖는다.

.. math ::
    \tan(\theta) = 2 \cot \beta \frac{M_1^2 \sin^2 \beta - 1}{M_1^2 (\gamma + \cos 2\beta) + 2} 

:math:`\theta-\beta-M` 관계식에 따라 경사 충격파의 각도 :math:`\beta` 는 2개의 해를 가질 수 있는데
일반적으로 각도가 작은 Weak 해의 경사각을 취한다.

경사 충격파는 충격파 각도에 수직한 속도 성분에 의한 수직 충격파 관계식에 따라
충격파 전/후 물성치를 결정할 수 있다.

충격파 전 수직 속도 성분에 의한 마하수는 다음과 같다.

.. math ::
    M_{n,1} = M_1 \sin\beta.

:mod:`compair.normal_shock` 관계식을 이용하여 충격파 후 수직방향 마하수 :math:`M_{n,2}` 를
계산할 수 있으며 이에 따라 충격파 후 마하수는 다음과 같다.

.. math ::
    M_2 = \frac{M_{n,2}}{\sin(\beta-\theta)}.

References
----------
[1] J. D. Anderson, "Fundamentals of Aerodynamics," McGrawHill. 2005.

Examples
--------
마하수 3인 유동장이 10도 각도를 갖는 쇄기에 부딛힐 경우 경사 충격파 이후 물성치는 다음과 같이 계산할 수 있다.

>>> from compair import oblique_shock as obq
>>> m, rho, p, p0, beta = obq.solve(3.0, 10)
>>> print("M2={:.2f}, rho={:.3f}, p={:.3f}, p0={:.3f}, beta={:.2f}".format(m, rho, p, p0, beta))
M2=2.51, rho=1.655, p=2.054, p0=0.963, beta=27.38
"""
from scipy.optimize import newton, minimize

import numpy as np 

import compair.normal_shock as nos


def _tangent_theta(beta_r, M, gamma=1.4):
    # Theta-Beta-M (Anderson book 9.23), radian
    return 2/np.tan(beta_r)*((M*np.sin(beta_r))**2 - 1)/(M**2*(gamma + np.cos(2*beta_r))+2)

def theta_beta(beta, M, gamma=1.4):
    r"""
    Theta-Beta-M 함수를 이용하여, :math:`\beta` , `M`을 이용하여
    :math:`Theta`를 계산한다. 

    Parameters
    ----------
    beta : float
        경사 충격파 각도 (degree)
    M : float
        충격파 전 마하수
    gamma : float, optional
        비열비
    
    Returns
    --------
    theta : float
        쇄기 각도 (degree)
    """
    # Convert deg to rad
    beta_r = np.deg2rad(beta)

    # With Theta-Beta-M 
    tan_theta = _tangent_theta(beta_r, M, gamma)

    return np.rad2deg(np.arctan(tan_theta))

def _beta_weak(M, theta, gamma=1.4):
    # Convert deg to rad
    theta_r = np.deg2rad(theta)

    f = lambda x : _tangent_theta(x, M, gamma) - np.tan(theta_r)

    # Solve Theta-Beta-M using Newton Method
    return newton(f, 1e-3)

def beta_weak(M, theta, gamma=1.4):
    r"""
    Theta-Beta-M 관계식을 수치적으로 계산하여
    마하수 `M` , 쐐기 각도 :math:`\beta` 일 때
    경사 충격파 각도 :math:`\beta` 를 계산한다.

    Parameters
    ----------
    M : float
        충격파 전 마하수
    theta : float
        쇄기 각도 (degree)
    gamma : float, optional
        비열비
    
    Returns
    --------
    beta : float
        경사 충격파 각도 (degree)
    """
    # Convert deg to rad
    return np.rad2deg(_beta_weak(M, theta, gamma))

def theta_max(M, gamma=1.4):
    r"""
    주어진 마하수 `M` 에 대해서 Theta-Beta-M 관계식에서
    Weak 해가 존재할 수 있는 최대 쇄기각 :math:`\theta` 를 계산한다.

    Parameters
    ----------
    M : float
        충격파 전 마하수
    gamma : float, optional
        비열비
    
    Returns
    --------
    theta : float
        최대 쇄기 각도 (degree)
    """
    # Maximize tangent theta
    f = lambda x : -_tangent_theta(x, M, gamma)
    res = minimize(f, 1e-3)
    tan_theta = -res.fun

    return np.rad2deg(np.arctan(tan_theta))

def _Mn1(M, beta_r, gamma=1.4):
    return M*np.sin(beta_r)

def Mn1(M, theta, gamma=1.4):
    r"""
    마하수 `M` , 쐐기각 :math:`\theta` 일때 발생한 경사충격파에
    수직인 마하수

    Parameters
    ----------
    M : float
        충격파 전 마하수
    theta : float
        쇄기 각도 (degree)
    gamma : float, optional
        비열비
    
    Returns
    --------
    Mn1 : float
        경사 충격파에 수직인 마하수
    """
    # Get beta in rad
    beta_r = _beta_weak(M, theta, gamma)

    return _Mn1(M, beta_r, gamma)

def M2(M, theta, gamma=1.4):
    r"""
    마하수 `M` , 쐐기각 :math:`\theta` 일때 발생한 경사충격파를
    지난 후 마하수

    Parameters
    ----------
    M : float
        충격파 전 마하수
    theta : float
        쇄기 각도 (degree)
    gamma : float, optional
        비열비
    
    Returns
    --------
    M2 : float
        경사 충격파후 마하수
    """
    # Get beta in rad
    beta_r = _beta_weak(M, theta, gamma)

    # Normal component of M1
    mn1= _Mn1(M, beta_r)

    # Normal shock relation
    mn2 = nos.mach(mn1, gamma)

    theta_r = np.deg2rad(theta)

    # Compute M2
    return mn2 / np.sin(beta_r - theta_r)


def solve(M, theta, gamma=1.4):
    """
    마하수 `M` , 쐐기각 :math:`\theta` 일때 발생한 경사충격파를
    지난 후 물성치 계산

    Parameters
    ----------
    M : float
        충격파 전 마하수
    theta : float
        쇄기 각도 (degree)
    gamma : float, optional
        비열비
    
    Returns
    --------
    m2 : float
        수직충격파 후 마하수
    rho2 : float
        수직충격파 전/후 밀도비
    p2 : float
        수직충격파 전/후 압력비
    p0ratio : float
        수직충격파 전/후 전압력비
    beta : float
        경사 충격파 각도 (degree)
    """
    if theta < theta_max(M):
        beta_r = _beta_weak(M, theta, gamma)
    else:
        print('Bow shock occured at M={:.3f}, theta={:.3f}'.format(M, theta))
        beta_r = np.pi/2

    # Get beta in rad
    beta = np.rad2deg(beta_r)

    # Normal component of M1
    mn1= _Mn1(M, beta_r)

    # Normal shock relation
    mn2, rho2, p2, p0ratio = nos.solve(mn1, gamma)

    # Compute M2
    theta_r = np.deg2rad(theta)
    m2 = mn2 / np.sin(beta_r - theta_r)

    return m2, rho2, p2, p0ratio, beta




