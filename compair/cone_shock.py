r"""
Cone 충격파 관계식
==================

3차원 Cone에서 발생한 충격파의 파각과 충격파 전/후 물성치, Cone 표면에서 물설치를 계산한다.
압축성 유체역학 교과서 [1] 에 자세한 유도과정이 설명되어 있다.

Note
-----
각도 :math:`\theta` 를 갖는 Cone에 대해 마하수 `M` 를 가질 때 다음과 같은 Taylor Maccoll 
방정식을 만족한다.

.. math ::
    \frac{\partial V_r}{\partial \theta} = V_{\theta}

.. math ::    
    \frac{\partial V_{\theta}}{\partial \theta} = \frac{V_{\theta}^2 V_r - (\gamma - 1) /2 (1- V_r^2 - V_{\theta}^2) (2 V_r + V_{\theta} \cot(\theta) }{ (\gamma - 1) / 2 (1 - V_r^2 - V_{\theta}^2) - V_{\theta}^2 }
    
이 방정식을 :math:`V_{\theta}` 가 양수일 때 까지만  수치적으로 적분하면 형상 각도 :math:`\delta` 를 구할 수 있다.
이 각도에 대해 경사 충격파 관계식을 이용하면 충격파 전/후 물성치를 구할 수 있다.

Cone 표면에서 마하수는 다음과 같다.

.. math ::
    M_{cone} = \sqrt{\frac{2}{\gamma-1}  V^2 / (1-V^2)}
    
충격파 후 부터 Cone 표면까지는 등엔트로피 변화이므로, Cone 마하수를 이용해 표면 물성치를 산출한다.

References
-----------
[1] A. H. Shapiro, "The Dynamics and Thermodynamics of Compressible Fluid Flow,"  MIT, 1953.

Examples
--------
마하수 3인 유동장이 10도 각도를 갖는 쇄기에 부딛힐 경우 경사 충격파 이후 물성치는 다음과 같이 계산할 수 있다.

>>> from compair import cone_shock as cone
>>> m, rho, p, p0, beta = cone.solve(2, 15)
>>> print("M2={:.2f}, rho={:.3f}, p={:.3f}, p0={:.3f}, beta={:.2f}".format(m, rho, p, p0, beta))
M2=1.71, rho=1.377, p=1.566, p0=0.998, beta=33.91
"""
from scipy.optimize import newton
from scipy.integrate import solve_ivp

import numpy as np

import compair.oblique_shock as obq
import compair.normal_shock as nos
import compair.isentropic as isen


def _taylor_maccoll(theta, y, gamma=1.4):
    # Taylor-Maccoll function
    # Source: https://www.grc.nasa.gov/www/k-12/airplane/coneflow.html
    v_r, v_theta = y
    dydt = [
        v_theta,
        (v_theta**2*v_r - (gamma - 1) / 2 * (1 - v_r**2 - v_theta**2)*(2*v_r + v_theta / np.tan(theta))) / ((gamma - 1) / 2 * (1 - v_r**2 - v_theta**2) - v_theta**2) 
    ]
    return dydt
    
    
def _integrate_tm(M, angle, theta, gamma=1.4):
    # Solve Oblique shock
    theta_max = obq.theta_max(M, gamma)
    
    if theta <= theta_max:
        beta = obq.beta_weak(M, theta, gamma)
        m2 = obq.M2(M, theta, gamma)
    else:
        beta = 90
        m2 = nos.mach(M, gamma)
    
    v = np.sqrt(((gamma-1)/2*m2**2)/(1 + (gamma-1)/2*m2**2))
    v_theta = -v*np.sin(np.deg2rad(beta - theta))
    v_r = v*np.cos(np.deg2rad(beta - theta))

    # Integrate over [beta, angle]
    sol = solve_ivp(_taylor_maccoll, np.deg2rad((beta, angle)), [v_r, v_theta])

    # Return v_theta
    return sol


def theta_eff(M, angle, gamma=1.4):
    r"""
    Cone 형상 각도 계산
    
    Parameters
    ------------
    M : float
        마하수
    angle : float
       Cone 반각 (degree)
    gamma : float
        비열비
        
    Returns
    --------
    theta_eff : float
        형상 각도 (degree)
    """
    # Newton solver s.t. v_theta = 0
    f = lambda x : _integrate_tm(M, angle, x, gamma=gamma).y[-1, -1]

    return newton(f, 1e-3)


def beta_weak(M, angle, gamma=1.4):
    r"""
    마하수 `M` , Cone 반각 :math:`\theta` 일때 Cone 충격파 각도 계산
    
    Parameters
    ----------
    M : float
        충격파 전 마하수
    angle : float
       Cone 반각 (degree)
    gamma : float, optional
        비열비
    
    Returns
    --------
    beta : float
        경사 충격파 각도 (degree)
    """
    # Compute theta
    theta = theta_eff(M, angle, gamma)
    
    # Return Shock angle
    return obq.beta_weak(M, theta, gamma)


def M2(M, angle, gamma=1.4):
    r"""
    마하수 `M` , Cone 반각 :math:`\theta` 일때 발생한 경사충격파를
    지난 후 마하수

    Parameters
    ----------
    M : float
        충격파 전 마하수
    angle : float
       Cone 반각 (degree)
    gamma : float, optional
        비열비
    
    Returns
    --------
    M2 : float
        경사 충격파후 마하수
    """
    # Compute theta_eff
    theta = theta_eff(M, angle, gamma)
    
    # Return M2
    return obq.M2(M, theta, gamma)


def _Mcone(M, angle, theta, gamma):
    # 무차원화된 속도 계산
    vec = _integrate_tm(M, angle, theta, gamma).y[:, -1]
    
    v = np.linalg.norm(vec)    
    phi = angle + np.rad2deg(np.arctan(vec[1] / vec[0]))
    
    return np.sqrt(2/(gamma-1)*(v**2 / (1- v**2))), phi
    
    
def Mcone(M, angle, gamma=1.4):
    r"""
    마하수 `M` , Cone 반각 :math:`\theta` 일때 Cone 표면 마하수

    Parameters
    ----------
    M : float
        충격파 전 마하수
    angle : float
       Cone 반각 (degree)
    gamma : float, optional
        비열비
    
    Returns
    --------
    M2 : float
        경사 충격파후 마하수
    """
    theta, _ = theta_eff(M, angle, gamma)
    
    return _Mcone(M, angle, theta, gamma)
    
    
def solve_shock(M, angle, gamma=1.4):
    r"""
    마하수 `M` , Cone 반각 :math:`\theta` 일때 발생한 경사충격파를
    지난 후 물성치 계산

    Parameters
    ----------
    M : float
        충격파 전 마하수
    angle : float
        쇄기 각도 (degree)
    gamma : float, optional
        비열비
    
    Returns
    --------
    m2 : float
        수직충격파 후 마하수
    rho2 : float
        수직충격파 전/후 밀도비
    p2 : float
        수직충격파 전/후 압력비
    p0ratio : float
        수직충격파 전/후 전압력비
    beta : float
        경사 충격파 각도 (degree)
    """
    theta = theta_eff(M, angle, gamma)

    return obq.solve(M, theta, gamma)
    
    
def solve_cone(M, angle, gamma=1.4):
    r"""
    마하수 `M` , Cone 반각 :math:`\theta` 일때 발생한 Cone 표면에서 물성치

    Parameters
    ----------
    M : float
        충격파 전 마하수
    angle : float
        쇄기 각도 (degree)
    gamma : float, optional
        비열비
    
    Returns
    --------
    mcone : float
        콘 표면 마하수
    rho2 : float
        콘 표면 밀도비
    p2 : float
        콘 표면 압력비
    p0ratio : float
        콘 표면 전압력비
    beta : float
        경사 충격파 각도 (degree)
    """
    mc, rhoc, pc, p0ratio, beta, _ = solve(M, angle, angle, gamma)
    return mc, rhoc, pc, p0ratio, beta
    
    
def solve(M, angle, psi, gamma=1.4):
    r"""
    마하수 `M` , Cone 반각 :math:`\theta` 일때 ray 각도 :math:`\psi` 발생한 Cone 표면에서 물성치

    Parameters
    ----------
    M : float
        충격파 전 마하수
    angle : float
        쇄기 각도 (degree)
    psi : float
    	Ray 각도 (degree)
    gamma : float, optional
        비열비
    
    Returns
    --------
    mcone : float
        콘 표면 마하수
    rho2 : float
        콘 표면 밀도비
    p2 : float
        콘 표면 압력비
    p0ratio : float
        콘 표면 전압력비
    beta : float
        경사 충격파 각도 (degree)
    phi : float
	    	유동 방향 (degree)
    """
    theta = theta_eff(M, angle, gamma)
    
     # 충격파 전/후 물성치 계산
    m2, rho2, p2, p0ratio, beta = obq.solve(M, theta, gamma)
    
    # Cone 마하수, 속도 방향
    mc, phi = _Mcone(M, psi, theta, gamma)
    
     # 밀도
    rhoc = rho2*isen.rho0_rho(m2)/isen.rho0_rho(mc)
    
     # 압력
    pc = p2*isen.p0_p(m2) / isen.p0_p(mc)
    
    return mc, rhoc, pc, p0ratio, beta, phi
    
    
    
    
