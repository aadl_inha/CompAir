"""
US Standard 1976 표준 대기 모델
====================================

US Standard 1976 표준 대기 모델로 ~86km 까지 대기 물성치를 산출함

Notes
-------
86km까지 대기를 7개의 Layer로 구성한다. 각 Layer 구간 별로 온도 구배를 고려하여
Hydrostatic 방정식을 계산하여, 바탕으로 밀도, 온도, 압력, 음속, 점도를 계산한다.

Examples
--------
고도 20km 에서 물성치는 다음과 같다.

>>> from compair.atmos1976 import atmos1976_at
>>> alt = 20
>>> density, pressure, temperature, asound, viscosity = atmos1976_at(alt)
>>> print(''' Standard Atmosphere 1976 at {:.2f} km
    Density        : {:15.5G} kg/m^3
    Pressure       : {:15.2f} Pa
    Temperature    : {:15.2f} K
    Speed of Sound : {:15.2f} m/s^2
    Viscosity      : {:15.6E} Pa s'''.format(alt, density, pressure, temperature, asound, viscosity))
 Standard Atmosphere 1976 at 20.00 km
 Density        :        0.088908 kg/m^3
 Pressure       :         5529.21 Pa
 Temperature    :          216.65 K
 Speed of Sound :          295.07 m/s^2
 Viscosity      :    1.421613E-05 Pa s
"""
import numpy as np


def atmos1976_at(alt):
    ''' 
    고도에 따른 표준 대기 (US Standard 1976) 물성치 계산

    Parameters
    ----------
    alt : float
        고도 (km))

    Returns
    -------
    density : float
        밀도 (kg/m^3)
    pressure : float
        압력 (Pa)
    temperature : float
        온도 (K)
    asound : float
        음속 (m/s^2)
    viscosity : float
        Dynamics 점도 (Pa s)
    '''
    t0, p0, rho0, a0 = 288.15, 101325.0, 1.225, 340.294
    
    # Compute ratio
    rhor, pr, tr = _air1976(alt)
    
    # Get density, pressure, temperature
    temperature = t0*tr
    pressure = p0*pr
    density = rho0*rhor
    
    # Speed of sound
    asound = a0*np.sqrt(tr)
    
    # Get viscosity
    viscosity = sutherland_mu(tr)
    
    return density, pressure, temperature, asound, viscosity
    

def geopot_alt(alt, rearth=6369.0):
    r''' 
    Geometric 고도 (Z)를 Geopotential 고도 (H)로 변환

    Parameters
    ----------
    alt : float
        Geometric 고도 (km)
    Rearth : float, optional
        지구 반지름, 기본값은 6369.0km.

    Returns
    -------
    H : float
        Geopotential 고도 (km)
    '''
    return alt*rearth/(rearth + alt)
    

def geometric_alt(alt, rearth=6369.0):
    r''' 
    Geopotential 고도 (H)를 Geometric 고도 (Z)로 변환

    Parameters
    ----------
    alt : float
        Geopotential 고도 (km)
    Rearth : float, optional
        지구 반지름, 기본값은 6369.0km.

    Returns
    -------
    Z : float
        Geometric 고도 (km))
    '''
    return alt*rearth/(rearth - alt)
    
    
def _air1976(alt, gmr = 34.163195):
    # Seven Layer model up to 86km
    air_layers = np.array([
        [ 0.0,  11.0, 20.0, 32.0, 47.0, 51.0, 71.0, 84.852 ], 
        [ 288.15, 216.65, 216.65, 228.65, 270.65, 270.65, 214.65, 186.946 ], 
        [ 1.0, 2.2336110E-1, 5.4032950E-2, 8.5666784E-3, 1.0945601E-3, 6.6063531E-4, 3.9046834E-5, 3.68501E-6 ], 
        [ -6.5, 0.0, 1.0, 2.8, 0.0, -2.8, -2.0, 0.0 ]
        ])
    
    
    tbase0 = air_layers[1,0]
    
    # Computet geopotential altitude
    h = geopot_alt(alt)
    
    # Find index
    if h  > 0.0:
        idx = np.searchsorted(air_layers[0], h) -1
    else:
        idx = 0
    
    # Get values 
    hbase, tbase, pbase, tgrad = air_layers.T[idx]
    
    # Computet temperature and ratio
    dh = h - hbase
    tlocal = tbase + tgrad*dh
    theta = tlocal / tbase0

    # Compute pressure ratio
    if abs(tgrad) < 1e-6:
        delta = pbase * np.exp(-gmr*dh/tbase)
    else:
        delta = pbase*np.power(tbase/tlocal, gmr/tgrad)

    # Computet Density ratio
    sigma = delta/ theta
    
    return sigma, delta, theta
    
    
def sutherland_mu(theta, t0=288.15, mu0=1.458e-6, suth=110.4):
    '''
    Sutherland law for viscosity

    온도에 따른 Dynamics 점도 계산

    Parameters
    ----------
    theta : float
        온도 비 (15C 대비 현재 온도)
    t0 : float, optional
        기준 온도, 기본값은 15C
    mu0 : float, optional
        기준 온도에서 점도, 기본값은 1.458e-6
    suth : float, optional
        Sutherland 관계식 계수, 기본값은 110.4

    Returns
    -------
    mu : float
        온도에 따른 Dynamic 점도
    '''
    t = t0*theta
    return mu0*t*np.sqrt(t)/(t+suth)


if __name__ == '__main__':
    alt = input(" Geometric Altitude (km) : ")
    z = geopot_alt(alt)
    print(" Geopotential Altitude is {:.2f} km".format(z))
    
    density, pressure, temperature, asound, viscosity = atmos1976_at(alt)
    print(''' Standard Atmosphere 1976 at {:.2f} km
    Density        : {:15.5G} kg/m^3
    Pressure       : {:15.2f} Pa
    Temperature    : {:15.2f} K
    Speed of Sound : {:15.2f} m/s^2
    Viscosity      : {:15.6E} Pa s'''.format(alt, density, pressure, temperature, asound, viscosity))

