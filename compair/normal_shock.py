r"""
수직 충격파 관계식
==================

수직 충격파 전/후 물성치를 계산하며, 압축성 유체역학 교과서 [1] 에 유도 과정이 설명되어 있다.


Notes
------
수직 충격파 후 마하수 및 전/후 밀도, 압력비는 식 압축성 유체역학 6장에서와 같이 유도할 수 있다 [1] .

.. math::
    M_2^2 = \frac{1+[(\gamma-1)/2]M_1^2}{\gamma M_1^2 - (\gamma-1)/2},
    
    \frac{\rho_2}{\rho_1} = \frac{(\gamma+1)M_1^2}{2+(\gamma-1)M_1^2},

    \frac{p_2}{p_1} = 1 + \frac{2\gamma}{\gamma+1}(M_1^2-1).

온도 비는 압력과 밀도비를 이용하여 다음과 같이 유도된다 [1] .

.. math ::
    \frac{T_2}{T_1} = \frac{p_2}{p_1} / \frac{\rho_2}{\rho_1}.

References
----------
[1] J. D. Anderson, "Fundamentals of Aerodynamics," McGrawHill. 2005.

Examples
--------
마하수 3인 유동장에서 수직 충격파가 발생했을 경우 물성치는 다음과 같이 계산할 수 있다.

>>> from compair import noraml_shock as nos
>>> m, rho, p, p0 = nos.solve(3.0)
>>> print("M2={:.2f}, rho={:.3f}, p={:.3f}, p0={:.3f}".format(m, rho, p, p0))
M2=0.48, rho=3.857, p=10.333, p0=0.328
"""
import numpy as np
import compair.isentropic as isen 


def mach(M, gamma=1.4):
    """
    수직 충격파 후 마하수

    Parameters
    ----------
    M : float
        충격파 전 마하수
    gamma : float, optional
        비열비
    
    Returns
    --------
    mach : float
        충격파 후 마하수
    """
    return np.sqrt((1 + 0.5*(gamma - 1)*M**2)/(gamma*M**2 - 0.5*(gamma-1)))


def rho(M, gamma=1.4):
    """
    수직 충격파 전/후 밀도비

    Parameters
    ----------
    M : float
        충격파 전 마하수
    gamma : float, optional
        비열비
    
    Returns
    --------
    rho : float
        충격파 전/후 밀도비

    Notes
    -----
    수직 충격파 전/후 밀도비는 압축성 유체역학 이론을 통해 유도된다.
    """
    return (gamma+1)*M**2/(2+(gamma-1)*M**2)


def p(M, gamma=1.4):
    """
    수직 충격파 전/후 압력비

    Parameters
    ----------
    M : float
        충격파 전 마하수
    gamma : float, optional
        비열비
    
    Returns
    --------
    p : float
        충격파 전/후 압력비
    """
    return 1 + 2*gamma/(gamma+1)*(M**2-1)


def t(M, gamma=1.4):
    """
    수직 충격파 전/후 온도비

    Parameters
    ----------
    M : float
        충격파 전 마하수
    gamma : float, optional
        비열비
    
    Returns
    --------
    t : float
        충격파 전/후 온도비
    """
    return p(M, gamma)/rho(M, gamma)


def p0(M, gamma=1.4):
    """
    수직 충격파 후 전압력

    Parameters
    ----------
    M : float
        충격파 전 마하수
    gamma : float, optional
        비열비
    
    Returns
    --------
    p0 : float
        충격파 후 전압력
    """
    p2 = p(M)
    m2 = mach(M)
    return isen.p0_p(m2, gamma)*p2


def p02_p01(M, gamma=1.4):
    """
    수직 충격파 후 전압력비

    Parameters
    ----------
    M : float
        충격파 전 마하수
    gamma : float, optional
        비열비
    
    Returns
    --------
    p0 : float
        충격파 후 전압력비
    """
    p01 = isen.p0_p(M, gamma)
    p02 = p0(M, gamma)
    return p02/p01


def solve(M, gamma=1.4):
    """
    수직 충격파 후 물성치 변화

    Parameters
    ----------
    M : float
        충격파 전 마하수
    gamma : float, optional
        비열비
    
    Returns
    --------
    m2 : float
        수직충격파 후 마하수
    rho2 : float
        수직충격파 전/후 밀도비
    p2 : float
        수직충격파 전/후 압력비
    p02 : float
        수직충격파 전/후 전압력비
    """
    # 수직 충격파 후 마하수
    m2 = mach(M, gamma)

    # 수직 충격파 전/후 밀도, 압력비
    rho2, p2 = rho(M, gamma), p(M, gamma)

    # 수직 충격파 전/후 전압력비
    p0ratio = p02_p01(M, gamma)

    # Return M2, rho2, p2, p02/p01
    return m2, rho2, p2, p0ratio
