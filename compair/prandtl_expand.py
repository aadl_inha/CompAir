r"""
Prandtl-Meyer 팽창파 관계식
==============================

Prandtl-Meyer 팽창파 관계식을 이용하여 유동이 
각도 :math:`\theta` 만큼 회전했을 경우 물성치를 계산한다.

Notes
-------
Prandtl Meyer 함수는 압축성 유체역학 8장 [1] 에 유도된 바와 같이 정의된다.

.. math ::
    \nu(M) = \sqrt{\frac{\gamma+1}{\gamma-1}} \tan^{-1} \sqrt{\frac{\gamma-1}{\gamma+1} (M^2-1)}
    -\tan^{-1} \sqrt{M^2-1}.

유동 회전 각도 :math:`\theta` 와 팽창파 전/후 마하수관에는 다음 관계식이 만족한다.

.. math ::
    \theta = \nu(M_2) - \nu(M_1).

팽창파를 지나면서 엔트로피가 증가되지 않으므로, :mod:`compair.isentropic` 관계식을 이용하여 
팽창파 전/후 물성치 변화를 계산한다.

References
----------
[1] J. D. Anderson, "Fundamentals of Aerodynamics," McGrawHill. 2005.

Examples
--------
마하수 3인 유동장이 10도 만큼 팽창할 경우

>>> from compair import prandtl_expand as expd
>>> m2 = expd.mach(3, 10)
>>> p2 = expd.p(3, 10)
>>> print("M2={:.2f}, p2={:.3f}".format(m2, p2))
M2=3.58, p2=0.431
"""
from scipy.optimize import newton

import numpy as np
import compair.isentropic as isen


def prandtl_meyer(M, gamma=1.4):
    """
    Prandtl Meyer 함수 nu 계산

    Parameters
    ----------
    M : float
        입구 마하수
    gamma : float, optional
        비열비. Default value is 1.4.

    Returns
    -------
    nu : float
        Prandtl-Meyer 함수.
    """
    gratio = np.sqrt((gamma+1)/(gamma-1))
    fm = np.sqrt(M**2 -1)
    rad = gratio*np.arctan(fm/gratio) - np.arctan(fm)
    return rad/np.pi*180


def mach(M1, theta, gamma=1.4):
    """
    마하수 M1인 유동이 theta 만큼 회전했을 때 마하수 계산

    Parameters
    ----------
    M1 : float
        입구 마하수
    theta : float
        유동 회전 각도
    gamma : float, optional
        비열비. Default value is 1.4.

    Returns
    -------
    mach : float
        팽창파를 지난 후 마하수
    """
    v1 = prandtl_meyer(M1, gamma)
    v2 = v1 + theta
    
    f = lambda M: prandtl_meyer(M, gamma) - v2
    return newton(f, M1)


def p(M1, theta, gamma=1.4):
    """
    마하수 M1인 유동이 theta 만큼 회전했을 때 압력 계산

    Parameters
    ----------
    M1 : float
        입구 마하수
    theta : float
        유동 회전 각도
    gamma : float, optional
        비열비. Default value is 1.4.

    Returns
    -------
    p : float
        팽창파를 지난 후 압력
    """
    M2 = mach(M1, theta)
    return isen.p0_p(M1, gamma)/isen.p0_p(M2, gamma)


def theta_p(pratio, M1, gamma=1.4):
    """
    압력비를 만족하도록 발생하는 팽창파 각도 계산

    Parameters
    ----------
    pratio : float
        팽창파를 전/후 압력비
    M1 : float
        입구 마하수
    gamma : float, optional
        비열비. Default value is 1.4.

    Returns
    -------
    theta : float
        유동 회전 각도
    """
    f = lambda theta: p(M1, theta, gamma) - pratio
    return newton(f, 0.1)
