r"""
등엔트로피 관계식
===================

등엔트로피 관계식을 이용하여 주어진 마하수에서 정체값 계산

Note
----

등엔트로피 유동에서는 다음 관계식을 만족한다 [1] .  

.. math::
    \frac{T_0}{T} = 1 + \frac{\gamma - 1}{2} M^2,


    \frac{p_0}{p} = \left(\frac{\rho_0}{\rho}\right)^\gamma 
    = \left(\frac{T_0}{T}\right)^{\gamma/(\gamma-1)}

References
----------
[1] J. D. Anderson, "Fundamentals of Aerodynamics," McGrawHill. 2005.

Examples
--------
마하수 3인 유동장에서 전압력 및 전온도는 다음과 같이 계산할 수 있다.

>>> from compair import isentropic as isen
>>> p0 = isen.p0_p(3)
>>> t0 = isen.t0_t(3)
>>> print("p0={:.2f}, T0={:.2f}".format(p0, t0))
p0=36.73, T0=2.80
"""
import numpy as np


# Isentropic relation (Anderson Book 8.4x)
def t0_t(M, gamma=1.4):
    """
    마하수 `M` 일때 정체 온도비

    Parameters
    ----------
    M : float
        마하수
    gamma : float, optional
        비열비
    
    Returns
    --------
    t0 : 정체 온도비
    """
    return 1 + 0.5*(gamma - 1)*M**2

def p0_p(M, gamma=1.4):
    """
    마하수 `M` 일때 정체 압력비

    Parameters
    ----------
    M : float
        마하수
    gamma : float, optional
        비열비
    
    Returns
    --------
    p0 : 정체 압력비
    """
    t = t0_t(M, gamma)
    return t**(gamma/(gamma-1))

def rho0_rho(M, gamma=1.4):
    """
    마하수 `M` 일때 정체 밀도비

    Parameters
    ----------
    M : float
        마하수
    gamma : float, optional
        비열비
    
    Returns
    --------
    rho0 : 정체 밀도비
    """
    t = t0_t(M, gamma)
    return t**(1/(gamma-1))
