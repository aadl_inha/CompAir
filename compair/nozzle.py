r"""
준 1차원 노즐 관계식
=====================

준 1차원 노즐에서 면적비에 따른 물성치 변화 및 유량비를 계산한다.
노즐 내부에서 엔트로피 변화는 없다고 가정한다.

Notes
------
Converge-Diverge 노즐에서 유동이 질식되었을 경우 노즐 목에서 마하수는 1이고
이때 윗첨자 `*` 로 표기한다. 

노즐을 질식시킬 수 있는 노즐 목 :math:`A^*` 에 대해서 면적 비율에 따른 
마하수는 다음과 같다 [1].

.. math ::
    \left ( \frac{A}{A^*} \right)^2 =
    \frac{1}{M^2}
    \left[  \frac{2}{\gamma+1} 
    \left( 1 + \frac{\gamma -1}{2}M^2
    \right)^{(\gamma+1)/(\gamma-1)}
    \right].

노즐 내에서 유량은 입구 마하수 `M` , 입구 면적 `A` , 전압력 :math:`p_0` , 
전온도 :math:`T_0` , 기체 상수 `R` 에 대해서 다음과 같이 계산한다 [2]. 

.. math ::
    \frac{\dot{m}}{A} = \sqrt{\frac{\gamma} {R}} \frac{p_0}{\sqrt{T_0}}
    \frac{M}{\left ( 1 + \frac{\gamma - 1}{2} M^2
    \right)^{\frac{\gamma+1}{2(\gamma-1)}}}

References
----------
[1] J. D. Anderson, "Fundamentals of Aerodynamics," McGrawHill. 2005.

[2] A. H. Shapiro, "The Dynamics and Thermodynamics of Compressible Fluid Flow." RPC, 1953
        
Examples
--------
마하수 1.5, 0.6일 때 목면적 대비 면적비는 다음과 같다.

>>> from compair import nozzle
>>> nozzle.area_ratio_at(1.5)
1.1761670524691357
>>> nozzle.area_ratio_at(0.6)
1.1881995061728399

목면적 비율이 1.18일 때 노즐내 마하수는 아음속 및 초음속일 때 다음과 같다.

>>> nozzle.mach_by_area_ratio(1.18, x0=0.1)
0.6070523888191025
>>> nozzle.mach_by_area_ratio(1.18, x0=1.1)
1.5056402461408018
"""
from scipy.optimize import newton

import numpy as np

import compair.isentropic as isen
import compair.normal_shock as shock
import compair.prandtl_expand as expand

      
def mdot(M, area=1, p0=1, t0=1, gamma=1.4, R=1):
    """
    노즐 내 유량 계산

    Parameters
    ----------
    M : float
        입구 마하수
    area : float
        입구 면적
    p0 : float
        전압력
    t0 : float
        전 온도
    gamma : float, optional
        비열비. Default value is 1.4.
    R : float
        Gas 상수

    Returns
    -------
    mdot : float
        질량 유량
    """
    mdot_a = np.sqrt(gamma/R/t0)*p0*M/(1+(gamma-1)/2*M**2)**((gamma+1)/2/(gamma-1))
    
    return mdot_a*area    


def area_ratio_at(M, gamma=1.4):
    """
    마하수 `M`일 때 목 면적 비율

    Parameters
    ----------
    M : float
        마하수
    gamma : float, optional
        비열비. Default value is 1.4.
    
    Returns
    -------
    area_ratio : float
        목면적 대비 면적비
    """
    return 1/M*np.sqrt((2/(gamma+1)*isen.t0_t(M, gamma))**((gamma+1)/(gamma-1)))


def mach_by_area_ratio(area, gamma=1.4, x0=0.1):
    """
    면적비 `area` 일 때 마하수 계산

    Parameters
    ----------
    area : float
        목면적 대비 면적비
    gamma : float, optional
        비열비. Default value is 1.4.
    x0 : float, optional
        초기 예측 값, 1 이하면 아음속, 1 이상이면 초음속 계산
    
    Returns
    -------
    M : float
        마하수
    """
    f = lambda M: area_ratio_at(M, gamma) - area
    
    # Solve newton-raphson
    return newton(f, x0)


# Nozzle state 6 (Isentropic expansion)
def me6(area, gamma=1.4):
    return mach_by_area_ratio(area, gamma)


def pe6(area, gamma=1.4, p0=1):
    Me6 = me6(area, gamma)
    return 1/isen.p0_p(Me6, gamma)*p0


def theta(area, pa, gamma=1.4, p0=1):
    Me = me6(area, gamma)
    pe = pe6(area, gamma, p0)
    
    return expand.theta_p(pa/pe, Me, gamma)


# Nozzle state 5 (Normal shock at the end)
def me5(area, gamma=1.4):
    Me = me6(area, gamma)
    return shock.mach(Me, gamma)


def pe5(area, gamma=1.4, p0=1):
    Me = me6(area, gamma)
    pe = pe6(area, gamma, p0)
    return pe*shock.p(Me, gamma)
